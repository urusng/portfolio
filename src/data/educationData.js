export const educationData = [
    {
        id: 1,
        institution: 'Can Tho University of Engineering and Technology',
        course: 'Computer Science',
        startYear: '2015',
        endYear: '2019',
        degreeGrade: 'Good'
    }
]