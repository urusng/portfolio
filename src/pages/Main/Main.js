import React from 'react'
import { Helmet } from 'react-helmet'

import { Navbar, Landing, Contacts, Footer, About, Education, Skills, Projects, Blog, DMCA } from '../../components'
import { headerData } from '../../data/headerData'

function Main() {
    return (
        <div>
            <Helmet>
                <title>{headerData.name} - Portfolio</title>
            </Helmet>

            <Navbar />
            <Landing />
            <About />
            <Skills />
            <Education />
            <Projects />
            {/*<Achievement />*/}
            <Blog />
            <Contacts />
            <Footer />
            <DMCA />
        </div>
    )
}

export default Main