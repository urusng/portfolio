import axios from 'axios'

axios.defaults.headers.post['Content-Type'] = 'application/json'

const ContactApi = {
    createContact: (contact) => axios({
        method: 'post',
        url: `/api/v1.0/contacts/`,
        data: contact
    }).then((res) => {
        if (res.status === 200) {
            return "SUCCESS"
        }
    }).catch(() => {
        return "ERROR"
    })
}

export default ContactApi