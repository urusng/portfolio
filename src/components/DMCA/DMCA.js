import React from 'react'
import './DMCA.css'
import { themeData } from "../../data/themeData";

function DCMA() {

    const theme = themeData.theme

    return (
        <div className="footer" style={{ backgroundColor: theme.secondary }}>
            <p style={{ color: theme.tertiary }}>
                <a href="65-b5b6-825f6d936533" title="DMCA.com Protection Status" class="dmca-badge"> <img src="https://images.dmca.com/Badges/DMCA_badge_grn_60w.png?ID=e6cc09bb-cd25-4165-b5b6-825f6d936533" alt="DMCA.com Protection Status" /></a>
            </p>
        </div>
    )
}

export default DCMA