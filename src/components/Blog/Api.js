import axios from 'axios'

const PhotoApi = {
    getPhoto: () => axios({
        method: 'get',
        url: `/photos/`,
    }).then(function (res) {
        if (res.status === 200) {
            return res.data
        }
        return []
    })
}

export default PhotoApi