import React from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";

import { BlogPage, Main, ProjectPage } from './pages'
import { BackToTop } from './components'
import ScrollToTop from './utils/ScrollToTop'

import './App.css'
import { themeData } from "./data/themeData";

const firebaseConfig = {
  apiKey: "AIzaSyBMxyauiPRMqP9RKVzUP0u-C9M_sACrZww",
  authDomain: "trinull-tmxgnp.firebaseapp.com",
  databaseURL: "https://trinull-tmxgnp.firebaseio.com",
  projectId: "trinull-tmxgnp",
  storageBucket: "trinull-tmxgnp.appspot.com",
  messagingSenderId: "578193233190",
  appId: "1:578193233190:web:c8f3a71889a14f91a6dde7"
};
// const app = initializeApp(firebaseConfig);
// const analytics = getAnalytics(app);

function App() {
  const app = initializeApp(firebaseConfig);
  getAnalytics(app);

  console.log("%cTaric Portfolio", `color:${themeData.theme.primary}; font-size:50px`);

  return (
    <div className="app">
      <Router>
        <ScrollToTop />
        <Switch>
          <Route path="/" exact component={Main} />
          <Route path="/portfolio/projects" exact component={ProjectPage} />
          <Route path="/portfolio/blogs" exact component={BlogPage} />

          <Redirect to="/" />
        </Switch>
      </Router>
      <BackToTop />
    </div>
  );
}

export default App;